package com.pwsztar;

public class Main {

    public static int sumNaturalNumbersIteratively(int limit) {

        int count = 0;
        for (int i = 1; i <= limit; ++i) {
            count += i;
        }

        return count;
    }

    public static void main(String[] args) {

    }

    static public int sumaRekurencja(int n) {
        if(n>0) {
            return n + sumaRekurencja(n-1);
        } else {
            return 0;
        }
    }

}

